**Zadani** \
Use scrapy framework to scrape the first 500 items (title, image url) from sreality.cz (flats, sell) and save it in the Postgresql database. Implement a simple HTTP server in python and show these 500 items on a simple page (title and image) and put everything to single docker-compose command so that I can just run "docker-compose up" in the Github repository and see the scraped ads on http://127.0.0.1:8080 page.

**Aktualni stav** \
Scrapy: Done \
Postgresql: Done \
HTTP Server: Done \
Docker: In progress

**Navod na pouziti** \
Jelikoz docker-compose zatim nefunguje korektne, je nutne provest instalaci/spusteni rucne. Pote je vystup dostupny na adrese http://localhost:8080/. Hlavni soubory scrapy casti jsou **flats.py** a **pipelines.py**.
 1. Nakonfigurovat postgresql databazi (napriklad pomoci pgAdmin) podle parametru v souboru /srealityscraper/srealityscraper/pipelines.py (popripade i jinak -> nutno vsak patricne upravit parametry v pipelines.py souboru).
 2. V konzoli spustit a nechat bezet soubor /server/server.py
 3. Ve vedlejsi konzoli spustit scrapy skript \
   a. cd /srealityscraper/srealityscraper \
   b. scrapy crawl sreality

